import numpy as np
import os
import random
import pickle
import pandas as pd


import torchvision
from torch.utils.data import Dataset

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from torchvision.io import read_image as imread
import torchvision.transforms as transforms




class ImageData(Dataset):

    def __init__(self, dir="../data/", mode='train', is_validation=False):
        self.is_validation = is_validation
        self.image_dir = dir + mode + "/"
        self.class_dir = dir + mode + ".csv"

        transform = transforms.Compose(
            [transforms.Normalize((0.5), (0.5))])

        y = np.array(pd.read_csv(self.class_dir)['label'].values)
        x = []
        
        for i in range(len(y)):
            image = imread(self.image_dir+str(i+1)+'.png', mode=torchvision.io.ImageReadMode.GRAY ).float()
            image = transform(image)
            x.append(image) 
            
           
        self.x_train = x[:int(len(x)*0.8)]
        self.y_train = y[:int(len(x)*0.8)]

        self.x_valid = x[int(len(x)*0.8):]
        self.y_valid = y[int(len(x)*0.8):]


        return


    def __getitem__(self, indx):
        if self.is_validation:
            images = self.x_valid[indx]
            labels = self.y_valid[indx]
        else:
            images = self.x_train[indx]
            labels = self.y_train[indx]
  
        return images, labels


    def __len__(self):
        'Denotes the total number of samples'
        if self.is_validation:
            return len(self.x_valid)
        else:
            return len(self.x_train)


    def show_images(self,img, is_rgb=False):
        if not is_rgb:
            img = img.squeeze()
        img = img / 2 + 0.5     # unnormalize
        npimg = img.numpy()
        plt.imshow(npimg,cmap='gray')
        plt.show()


if __name__ == "__main__":

    data = ImageData(dir="../../data/")