import torch
from torchsummary import summary

from model.conv_net import ConvNet


#input_size, channels, kernels, strides, paddings, fc_layers
config = {
    "input_size": [28,28],
    "channels": [1,6,16],
    "kernels":[5,5],
    "strides": [1,1],
    "paddings": [0,0],
    "fc_layers":[120,84,10], # need to check the number of classes in the dataset
    "learning_rate": 0.001,
    "lr_decay":0.7,
    "num_epochs":200,
    "batch_size":4,
    "device":torch.device("cuda:0" if torch.cuda.is_available() else "cpu"),
    "num_classes":10
}

model = ConvNet(**config)
summary(model, (1, 28, 28))

model.train_model()
