import torch
from torch.optim import Adam
import torch.nn.functional as F
from dataloader.dataset import ImageData
from torch.utils.data import DataLoader
from tqdm import tqdm

class ConvNet(torch.nn.Module):

    def __init__(self, input_size, channels, kernels, strides, paddings, fc_layers, **kwargs):


        super(ConvNet, self).__init__()

        self.input_size = input_size
        self.config = kwargs

        self.data = ImageData()
        
        assert len(channels)-1 == len(kernels) == len(strides) == len(paddings)

        net_layers = []
        output_size = input_size[0]
        
        for i in range(len(kernels)):
            net_layers.append(torch.nn.Conv2d(channels[i], channels[i+1], kernels[i], strides[i], paddings[i] ))
            net_layers.append(torch.nn.ReLU())
            output_size = self.conv_output_size(output_size, kernels[i], paddings[i], strides[i])
            net_layers.append(torch.nn.MaxPool2d(2, 2))
            output_size = self.conv_output_size(output_size, 2, 0, 2)

        net_layers.append(torch.nn.Flatten())
      
        fc_layers = [output_size**2*channels[-1]] + fc_layers

        for i in range(len(fc_layers)-1):
            net_layers.append(torch.nn.Linear(fc_layers[i], fc_layers[i+1]))
            if i < len(fc_layers) - 2:
                net_layers.append(torch.nn.ReLU())

        # net_layers.append(torch.nn.Softmax())

        def init_weights(m):
            if isinstance(m, torch.nn.Linear) or isinstance(m, torch.nn.Conv2d) :
                torch.nn.init.xavier_uniform(m.weight)
                m.bias.data.fill_(0.01)

        
        self.network = torch.nn.Sequential(*net_layers)
        self.network.apply(init_weights)

        self.network.to(self.config['device'])

        self.network_opt = Adam(self.parameters(), lr=kwargs["learning_rate"], eps=1e-3)
        self.lr_scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer=self.network_opt, gamma=kwargs["lr_decay"])


        self.loss = torch.nn.CrossEntropyLoss()


    def conv_output_size(self,input_size, kernel_size, padding, stride):
        size = (input_size-kernel_size + 2*padding)/stride + 1
        assert size % 1 == 0
        return int(size)



    def forward(self, x):
        return self.network(x)



    def train_model(self):

        for epoch in range(self.config['num_epochs']):
            stats = {}
            stats['loss'] = 0
            self.train()

            loader = DataLoader(
                self.data,
                batch_size=self.config["batch_size"],
                num_workers=1,
                shuffle=True
            )
       
            progress_bar = tqdm(loader, desc='| Epoch {:03d}'.format(epoch), leave=False, disable=False)

            for i, (images, labels) in enumerate(progress_bar):
                images, labels = images.to(self.config["device"]), labels.to(self.config["device"])
                
                pred = self(images.float())

             

                # labels = F.one_hot(labels, num_classes=self.config['num_classes']).float()/
     
                loss = self.loss(pred, labels)
       
                loss.backward()
                self.network_opt.step()
                self.network_opt.zero_grad()
                stats['loss'] += loss.cpu().item()

                
              

                progress_bar.set_postfix({key: '{:.4g}'.format(value / (i + 1)) for key, value in stats.items()}, refresh=True)


                
            self.lr_scheduler.step()



        

        return




    def validate_model(self):

        return



