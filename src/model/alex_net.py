
from multiprocessing import pool
import torch


class AlexNet(torch.nn.Module):

    def __init__(self,input_size, conv_layers, pooling_layers, fc_layers, num_classes, **kwargs):
        """
        as specified in: https://proceedings.neurips.cc/paper/2012/file/c399862d3b9d6b76c8436e924a68c45b-Paper.pdf

        note: input size is incorrectly specified in the paper, should be 227x227x3
        note: paper does not specifiy the padding required to get correct output dims for fc layers
        note: https://medium.com/analytics-vidhya/the-architecture-implementation-of-alexnet-135810a3370
        note: the above link defines some padding on layers however this also create the incorrect output dims for the fc layers 
        """
        super(AlexNet, self).__init__()

        self.input_size = input_size
        self.conv_layers = conv_layers
        self.pooling_layers = pooling_layers
        self.fc_layers = fc_layers

        net_layers = []
        output_size = input_size[0]

        output_size = self.conv_output_size(output_size, conv_layers[0].kernel_size,conv_layers[0].padding,conv_layers[0].stride) #c1
        output_size = self.conv_output_size(output_size, pooling_layers[0].kernel_size, pooling_layers[0].padding, pooling_layers[0].stride) #p1

        output_size = self.conv_output_size(output_size, conv_layers[1].kernel_size ,conv_layers[1].padding,conv_layers[1].stride) #c2
        output_size = self.conv_output_size(output_size, pooling_layers[1].kernel_size,pooling_layers[1].padding, pooling_layers[1].stride) #p2

        output_size = self.conv_output_size(output_size, conv_layers[2].kernel_size,conv_layers[2].padding,conv_layers[2].stride) #c3

        output_size = self.conv_output_size(output_size, conv_layers[3].kernel_size,conv_layers[3].padding,conv_layers[3].stride) #c4

        output_size = self.conv_output_size(output_size, conv_layers[4].kernel_size,conv_layers[4].padding,conv_layers[4].stride) #c5
        output_size = self.conv_output_size(output_size, pooling_layers[4].kernel_size,pooling_layers[4].padding, pooling_layers[4].stride) #p5

        print(output_size**2*256)
        assert output_size**2*256 == 4096

        #conv1
        net_layers.append(torch.nn.Conv2d(input_size[2],conv_layers[0].channels,conv_layers[0].kernel_size, conv_layers[0].stride,conv_layers[0].padding))
        net_layers.append(torch.nn.ReLU)
        net_layers.append(LocalResponseNorm())
        net_layers.append(torch.nn.MaxPool2d(pooling_layers[0].kernel_size,pooling_layers[0].stride))

        #conv2
        net_layers.append(torch.nn.Conv2d(conv_layers[0].channels,conv_layers[1].channels,conv_layers[1].kernel_size, conv_layers[1].stride,conv_layers[1].padding, groups=conv_layers[1].groups))
        net_layers.append(torch.nn.ReLU)
        net_layers.append(LocalResponseNorm())
        net_layers.append(torch.nn.MaxPool2d(pooling_layers[1].kernel_size,pooling_layers[1].stride))

        #conv3,4,5
        net_layers.append(torch.nn.Conv2d(conv_layers[1].channels,conv_layers[2].channels,conv_layers[2].kernel_size, conv_layers[2].stride,conv_layers[2].padding, groups=conv_layers[2].groups))
        net_layers.append(torch.nn.ReLU)
        net_layers.append(torch.nn.Conv2d(conv_layers[2].channels,conv_layers[3].channels,conv_layers[3].kernel_size, conv_layers[3].stride,conv_layers[3].padding, groups=conv_layers[3].groups))
        net_layers.append(torch.nn.ReLU)
        net_layers.append(torch.nn.Conv2d(conv_layers[3].channels,conv_layers[4].channels,conv_layers[4].kernel_size, conv_layers[4].stride,conv_layers[4].padding, groups=conv_layers[4].groups))
        net_layers.append(torch.nn.ReLU)
        net_layers.append(torch.nn.MaxPool2d(pooling_layers[4].kernel_size,pooling_layers[4].stride))

        #fc net
        net_layers.append(torch.nn.Flatten())
        net_layers.append(torch.nn.Linear(output_size,output_size))
        net_layers.append(torch.nn.ReLU)
        net_layers.append(torch.nn.Linear(output_size,num_classes))


 

        return


    def conv_output_size(self,input_size, kernel_size, padding, stride):
        size = (input_size-kernel_size + 2*padding)/stride + 1
        assert size % 1 == 0
        print(size)
        return int(size)


    def forward():

        return




class LocalResponseNorm(torch.nn.Module):

    def __init__(self):


        return



    def forward():
        """
        prevent unbounded avtivations from becoming to large: results of relu
        intra - normalized over single channel
        intra - normalized over k channels
        good refrence explanation: https://towardsdatascience.com/difference-between-local-response-normalization-and-batch-normalization-272308c034ac
        """

        return



if __name__ == "__main__":

    # model = AlexNet([227,227])
    pass