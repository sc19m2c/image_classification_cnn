import torch
from model.alex_net import AlexNet

class ConvParams():

    def __init__(self, channels, kernel_size, stride, padding, groups):

        self.channels = channels
        self.kernel_size = kernel_size
        self.stride = stride
        self.padding = padding
        self.groups = groups



c1 = ConvParams(96, 11, 4, 0, 1)
p1 = ConvParams(None, 3,2,0,None)

c2 = ConvParams(256, 5, 1, 2, 2)
p2 = ConvParams(None, 3,2,0,None)

c3 = ConvParams(384, 3,1,1,2)
c4 = ConvParams(384, 3,1,0,2)
c5 = ConvParams(384, 3,1,0,2)
p5 = ConvParams(None, 3,2,0,None)

convs = [c1,c2,c3,c4,c5]
pools = [p1,p2,None, None, p5]


config = {
    "input_size": [227,227,3],
    "conv_layers": convs,
    "pooling_layers": pools,
    "fc_layers":[4096,1000],
    "learning_rate": 0.001,
    "lr_decay":0.7,
    "num_epochs":200,
    "batch_size":500,
    "device":torch.device("cuda:0" if torch.cuda.is_available() else "cpu"),
    "num_classes":1000
}


model = AlexNet(**config)