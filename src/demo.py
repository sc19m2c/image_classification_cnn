import torch

from model.conv_net import ConvNet
from dataloader.dataset import ImageData

#input_size, channels, kernels, strides, paddings, fc_layers
config = {
    "input_size": [28,28],
    "channels": [1,6,16],
    "kernels":[5,5],
    "strides": [1,1],
    "paddings": [0,0],
    "fc_layers":[120,84,10], # need to check the number of classes in the dataset
    "learning_rate": 1e-3,
    "lr_decay":0.5
}

data = ImageData()
model = ConvNet(**config)

sample = data.x
sample = torch.Tensor(sample)
print(model(sample))